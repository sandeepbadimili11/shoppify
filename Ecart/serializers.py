from Ecart.models import orders, ordersItems, Products
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = orders
        fields = ('__all__')


class Orders_itemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ordersItems
        fields = ('__all__')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields='__all__'
    
        


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        Token.objects.create(user=user)
        return user
class LoginUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']
        extra_kwargs={'password': {'write_only': True}}
    # def create(self, validated_data):
    #     user = User(username=validated_data['username'])
    #     user.set_password(validated_data['password'])
        
    #     user.save()
        
    #     return user

