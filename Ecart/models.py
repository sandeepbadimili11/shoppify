from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Products(models.Model):
    Title = models.CharField(max_length=40)
    Description = models.TextField(max_length=100)
    image = models.ImageField(upload_to='photos')
    price = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add=True)
    Updated_At = models.DateField(auto_now_add=True)
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        return f"{self.Title}"


PAYMENT_CHOICES = (
    ("cash", "cash"),
    ("upi", "upi"),
    ("paytm", "paytm"),
    ("card", "card"),
)
STATUS_CHOICES = (
    ("new", "new"),
    ("paid", "paid"),
)


class orders(models.Model):

    userId = models.ForeignKey(User, on_delete=models.CASCADE)
    product_id=models.ManyToManyField(Products)
    Total = models.IntegerField()
    Created_At = models.DateTimeField(auto_now_add=True)
    Updated_At = models.DateField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    mode_of_payment = models.CharField(max_length=20, choices=PAYMENT_CHOICES)

    def __str__(self):
        return f"{self.userId}"


class ordersItems(models.Model):
    # user=models.ForeignKey()
    orderId = models.ForeignKey(orders, on_delete=models.CASCADE)
    productName= models.ForeignKey(Products, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    price = models.IntegerField()

    def __str__(self):
        return str(self.productId)
